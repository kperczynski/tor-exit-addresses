package perczynski.kamil.torexitaddresses;

import org.springframework.stereotype.Component;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.TestExecutionListener;

@Component
public class TestMethodExecutedListener implements TestExecutionListener {

    @Override
	public void afterTestMethod(TestContext testContext) {
        testContext.getApplicationContext()
                .publishEvent(new TestMethodExecutedEvent());
	}
}

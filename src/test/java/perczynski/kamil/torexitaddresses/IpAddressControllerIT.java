package perczynski.kamil.torexitaddresses;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import perczynski.kamil.torexitaddresses.parser.ExitAddress;
import perczynski.kamil.torexitaddresses.parser.ExitNode;

import java.time.Instant;
import java.util.Collections;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class IpAddressControllerIT extends TorExitAddressesIT {

    @Autowired
    private MockMvc mvc;
    @Autowired
    private ExitAddressesRepository addressesRepository;
    @Autowired
    private ExitNodeHelper exitNodeHelper;

    @Test
    public void testIpAddressNotFoundWithHeadRequest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.head("/123.0.0.1"))
                .andExpect(status().isNotFound())
                .andExpect(content().bytes(new byte[0]));
    }

    @Test
    public void testIpAddressNotFoundWithGetMethod() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/123.0.0.1"))
                .andExpect(status().isNotFound())
                .andExpect(content().bytes(new byte[0]));
    }

    @Test
    @DisplayName("Test lookup by ip with GET method")
    public void testFindByIpWithHeadMethod() throws Exception {
        // given:
        String nodeIp = ExitNodeHelper.randomIp();
        ExitNode exitNode = exitNodeHelper.builder()
                .addresses(Collections.singleton(
                        new ExitAddress(nodeIp, Instant.now())
                ))
                .build();
        addressesRepository.save(Collections.singleton(exitNode));

        // when & then:
        mvc.perform(MockMvcRequestBuilders.get("/{ip}", nodeIp))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].node_id").value(exitNode.getNodeId()))
                .andExpect(jsonPath("$[0].addresses[0].ip").value(nodeIp));
    }
}
package perczynski.kamil.torexitaddresses.actuator;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import perczynski.kamil.torexitaddresses.ExitAddressesRepository;
import perczynski.kamil.torexitaddresses.TorExitAddressesIT;
import perczynski.kamil.torexitaddresses.parser.ExitAddress;
import perczynski.kamil.torexitaddresses.parser.ExitNode;

import java.time.Instant;
import java.util.Collections;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.head;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class StatusControllerIT extends TorExitAddressesIT {

    @Autowired
    private MockMvc mvc;
    @Autowired
    private ExitAddressesRepository addressesRepository;

    @Test
    public void testHeadStatusRequest() throws Exception {
        mvc.perform(head("/status"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(new byte[0]));
    }

    @Test
    public void testGetStatusRequest() throws Exception {
        addressesRepository.save(Collections.singleton(
                new ExitNode("node-1", Instant.now(), Instant.now(), Collections.singleton(
                        new ExitAddress("127.0.0.1", Instant.now())
                ))
        ));

        mvc.perform(get("/status"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.tor_exit_nodes_count").value(1));
    }
}
package perczynski.kamil.torexitaddresses;

import org.springframework.context.annotation.Primary;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import perczynski.kamil.torexitaddresses.parser.ExitNode;

import java.util.*;

@Component
@Primary
public class TestExitAddressesRepository implements ExitAddressesRepository {

    private final CachedExitAddressesRepository knownExitAddresses;

    public TestExitAddressesRepository(CachedExitAddressesRepository knownExitAddresses) {
        this.knownExitAddresses = knownExitAddresses;
    }

    @EventListener(TestMethodExecutedEvent.class)
    public void onTestMethodExecuted() {
        knownExitAddresses.save(Collections.emptySet());
    }

    @Override
    public List<ExitNode> findByIp(String ip) {
        return knownExitAddresses.findByIp(ip);
    }

    @Override
    public void save(Set<ExitNode> exitNodes) {
        knownExitAddresses.save(exitNodes);
    }

    @Override
    public int count() {
        return knownExitAddresses.count();
    }
}

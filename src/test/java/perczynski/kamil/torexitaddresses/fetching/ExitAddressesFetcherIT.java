package perczynski.kamil.torexitaddresses.fetching;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureMockRestServiceServer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import perczynski.kamil.torexitaddresses.ExitAddressesRepository;
import perczynski.kamil.torexitaddresses.ExitNodeHelper;
import perczynski.kamil.torexitaddresses.TorExitAddressesIT;
import perczynski.kamil.torexitaddresses.parser.ExitAddress;
import perczynski.kamil.torexitaddresses.parser.ExitNode;

import java.time.Instant;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

public class ExitAddressesFetcherIT extends TorExitAddressesIT {

    @Autowired
    private ExitAddressesFetcher addressesFetcher;
    @Autowired
    private TorUrls torUrls;
    @Autowired
    private ExitAddressesRepository addressesRepository;
    @Autowired
    private MockRestServiceServer mockRestServiceServer;
    @Autowired
    private ExitNodeHelper exitNodeHelper;

    @Test
    public void testFetchExitAddresses() {
        // given:
        mockRestServiceServer.expect(MockRestRequestMatchers.requestTo(torUrls.exitAddressesUrl().toUriString()))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(new ClassPathResource("/exit-addresses"), MediaType.TEXT_PLAIN));

        // when:
        addressesFetcher.fetchExitAddresses();

        // then:
        assertThat(addressesRepository.count()).isEqualTo(3);
    }

    @Test
    public void testFetchFailurePreservesPastRecords() {
        // given:
        String nodeIp = ExitNodeHelper.randomIp();
        mockRestServiceServer.expect(MockRestRequestMatchers.requestTo(torUrls.exitAddressesUrl().toUriString()))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.NOT_FOUND));
        ExitNode exitNode = exitNodeHelper.builder()
                .addresses(Collections.singleton(
                        new ExitAddress(nodeIp, Instant.now())
                ))
                .build();
        addressesRepository.save(Collections.singleton(exitNode));

        // when:
        addressesFetcher.fetchExitAddresses();

        // then:
        assertThat(addressesRepository.count()).isEqualTo(1);
        assertThat(addressesRepository.findByIp(nodeIp)).containsOnly(exitNode);
    }

}
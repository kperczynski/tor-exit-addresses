package perczynski.kamil.torexitaddresses;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import perczynski.kamil.torexitaddresses.parser.ExitAddress;
import perczynski.kamil.torexitaddresses.parser.ExitNode;
import perczynski.kamil.torexitaddresses.parser.ExitNodesParser;
import perczynski.kamil.torexitaddresses.parser.TorDNSELSyntaxException;

import java.time.Instant;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.Assertions.tuple;

public class ExitNodesParserTest {

    @Test
    public void testParserCorrectlyRecognisesNodes() {
        String dnsEl = "ExitNode 0011BD2485AD45D984EC4159C88FC066E5E3300E\n" +
                "Published 2018-10-09 23:09:43\n" +
                "LastStatus 2018-10-10 00:03:39\n" +
                "ExitAddress 162.247.74.201 2018-10-10 00:07:09\n" +
                "ExitAddress 162.247.74.202 2018-10-10 00:07:09\n" +
                "ExitNode 0011BD2485AD45D984EC4159C88FC066E5E3300F\n" +
                "Published 2018-10-09 23:09:43\n" +
                "LastStatus 2018-10-10 00:03:39\n" +
                "ExitAddress 162.247.74.201 2018-10-10 00:07:09";

        Set<ExitNode> exitNodes = new ExitNodesParser(dnsEl).parse();

        assertThat(exitNodes).hasSize(2);
    }

    @Test
    public void testParseNodeProperties() {
        String dnsEl = "ExitNode 0011BD2485AD45D984EC4159C88FC066E5E3300E\n" +
                "Published 2018-10-09 23:09:43\n" +
                "LastStatus 2018-10-10 00:03:39\n" +
                "ExitAddress 162.247.74.201 2018-10-10 00:07:09";

        Set<ExitNode> exitNodes = new ExitNodesParser(dnsEl).parse();

        ExitNode node = exitNodes.iterator().next();
        SoftAssertions.assertSoftly(assertion -> {
            assertion.assertThat(node.getNodeId()).isEqualTo("0011BD2485AD45D984EC4159C88FC066E5E3300E");
            assertion.assertThat(node.getPublished()).isEqualTo(Instant.parse("2018-10-09T23:09:43Z"));
            assertion.assertThat(node.getLastStatus()).isEqualTo(Instant.parse("2018-10-10T00:03:39Z"));
        });
    }

    @Test
    public void testParseNodeAddresses() {
        String dnsEl = "ExitNode 0011BD2485AD45D984EC4159C88FC066E5E3300E\n" +
                "Published 2018-10-09 23:09:43\n" +
                "LastStatus 2018-10-10 00:03:39\n" +
                "ExitAddress 162.247.74.201 2018-10-10 00:07:09\n" +
                "ExitAddress 162.247.71.201 2018-10-10 00:10:09";

        Set<ExitNode> exitNodes = new ExitNodesParser(dnsEl).parse();

        ExitNode node = exitNodes.iterator().next();
        assertThat(node.getAddresses())
                .extracting(ExitAddress::getIp, ExitAddress::getTestTime)
                .containsExactlyInAnyOrder(
                        tuple("162.247.74.201", Instant.parse("2018-10-10T00:07:09Z")),
                        tuple("162.247.71.201", Instant.parse("2018-10-10T00:10:09Z"))
                );
    }

    @Test
    public void testNodeWithoutAddresses() {
        String dnsEl = "ExitNode 0011BD2485AD45D984EC4159C88FC066E5E3300E\n" +
                "Published 2018-10-09 23:09:43\n" +
                "LastStatus 2018-10-10 00:03:39";

        Throwable throwable = catchThrowable(() -> new ExitNodesParser(dnsEl).parse());

        assertThat(throwable)
                .isInstanceOf(TorDNSELSyntaxException.class);
    }

    @Test
    public void testDocumentWithIllegalAddressLine() {
        String dnsEl = "ExitNode 0011BD2485AD45D984EC4159C88FC066E5E3300E\n" +
                "Published 2018-10-09 23:09:43\n" +
                "LastStatus 2018-10-10 00:03:39\n" +
                "ExitAddress 162.247.74.201 2018-10-10 00:07:09\n" +
                "abc 162.247.74.202 2018-10-10 00:07:09\n";

        Throwable throwable = catchThrowable(() -> new ExitNodesParser(dnsEl).parse());

        assertThat(throwable)
                .isInstanceOf(TorDNSELSyntaxException.class);
    }

    @Test
    public void testDocumentWithIllegalPublishedLine() {
        String dnsEl = "ExitNode 0011BD2485AD45D984EC4159C88FC066E5E3300E\n" +
                "Published 2018-10-09 23:09:43\n" +
                "LastStatus 2018-10-10 00:03:39\n" +
                "ExitAddress 162.247.74.201 2018-10-10 00:07:09\n" +
                "Published 2018-10-09 23:09:43\n";

        Throwable throwable = catchThrowable(() -> new ExitNodesParser(dnsEl).parse());

        assertThat(throwable)
                .isInstanceOf(TorDNSELSyntaxException.class);
    }

    @Test
    public void testDocumentWithIpv6Address() {
        String dnsEl = "ExitNode 0011BD2485AD45D984EC4159C88FC066E5E3300E\n" +
                "Published 2018-10-09 23:09:43\n" +
                "LastStatus 2018-10-10 00:03:39\n" +
                "ExitAddress 2001:db8:0:0:1::1 2018-10-10 00:07:09\n" +
                "Published 2018-10-09 23:09:43\n";

        Throwable throwable = catchThrowable(() -> new ExitNodesParser(dnsEl).parse());

        assertThat(throwable)
                .isInstanceOf(TorDNSELSyntaxException.class);
    }

}
package perczynski.kamil.torexitaddresses.actuator;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import perczynski.kamil.torexitaddresses.ExitAddressesRepository;

@RestController
@RequestMapping("/status")
@AllArgsConstructor
public class StatusController {

    private final ExitAddressesRepository addressesRepository;

    @GetMapping
    public StatusInfo statusInfo() {
        int count = addressesRepository.count();
        return new StatusInfo(count);
    }

}

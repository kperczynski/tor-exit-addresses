package perczynski.kamil.torexitaddresses.actuator;

import lombok.Value;

@Value
public class StatusInfo {

    private final int torExitNodesCount;

}

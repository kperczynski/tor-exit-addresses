package perczynski.kamil.torexitaddresses.web;

import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Map;

@RestControllerAdvice(annotations = RestController.class)
public class RestErrorHandler {

    @ExceptionHandler(ConversionFailedException.class)
    public ResponseEntity<Object> illegalArgument(ConversionFailedException e) {
        return ResponseEntity.badRequest()
                .body(Map.of(
                        "error", e.getCause().getMessage()
                ));
    }

}

package perczynski.kamil.torexitaddresses;

public class ExitAddressNotFoundException extends RuntimeException {
    private final String ip;

    public ExitAddressNotFoundException(String ip) {
        this.ip = ip;
    }

    public String getIp() {
        return ip;
    }
}

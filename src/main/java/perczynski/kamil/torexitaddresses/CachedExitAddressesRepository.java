package perczynski.kamil.torexitaddresses;

import org.springframework.stereotype.Component;
import perczynski.kamil.torexitaddresses.parser.ExitNode;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class CachedExitAddressesRepository implements ExitAddressesRepository {

    private volatile Set<ExitNode> allNodes = Collections.emptySet();

    @Override
    public List<ExitNode> findByIp(String ip) {
        // it appears it's possible for 2 nodes to have common IP address
        return allNodes.stream()
                .filter(node -> node.getAddresses().stream()
                        .anyMatch(address -> address.getIp().equals(ip)))
                .collect(Collectors.toList());
    }

    @Override
    public void save(Set<ExitNode> nodes) {
        this.allNodes = Collections.unmodifiableSet(nodes);
    }

    @Override
    public int count() {
        return allNodes.size();
    }

}

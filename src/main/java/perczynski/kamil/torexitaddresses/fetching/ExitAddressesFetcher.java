package perczynski.kamil.torexitaddresses.fetching;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import perczynski.kamil.torexitaddresses.ExitAddressesRepository;
import perczynski.kamil.torexitaddresses.parser.ExitNode;
import perczynski.kamil.torexitaddresses.parser.ExitNodesParser;

import java.util.Set;

@Component
@Slf4j
public class ExitAddressesFetcher {

    private final TorUrls torUrls;
    private final RestTemplate httpClient;
    private final ExitAddressesRepository exitAddressesRepository;

    public ExitAddressesFetcher(@ExitAddresses RestTemplate httpClient,
                                TorUrls torUrls,
                                ExitAddressesRepository exitAddressesRepository) {
        this.torUrls = torUrls;
        this.httpClient = httpClient;
        this.exitAddressesRepository = exitAddressesRepository;
    }

    @Scheduled(initialDelay = 5000, fixedRate = 30 * 60 * 1000)
    public void fetchExitAddresses() {
        log.info("Started fetching TOR exit addresses");
        try {
            UriComponents addressesUrl = torUrls.exitAddressesUrl().build();
            String rawAddressesDocument = httpClient.getForObject(addressesUrl.toUri(), String.class);

            Set<ExitNode> exitNodes = new ExitNodesParser(rawAddressesDocument).parse();
            log.info("Fetched {} TOR exit addresses", exitNodes.size());
            exitAddressesRepository.save(exitNodes);
        } catch (RestClientException e) {
            log.error("Failed to fetch TOR exit addresses.", e);
        }
    }
}

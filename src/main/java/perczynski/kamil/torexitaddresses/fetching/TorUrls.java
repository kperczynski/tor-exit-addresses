package perczynski.kamil.torexitaddresses.fetching;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class TorUrls {

    private final String exitAddressesUrl;

    public TorUrls(@Value("${tor.exit-addresses-url}") String exitAddressesUrl) {
        this.exitAddressesUrl = exitAddressesUrl;
    }

    public UriComponentsBuilder exitAddressesUrl() {
        return UriComponentsBuilder.fromHttpUrl(exitAddressesUrl);
    }
}

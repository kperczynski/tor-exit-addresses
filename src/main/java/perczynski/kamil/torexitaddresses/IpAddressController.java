package perczynski.kamil.torexitaddresses;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import perczynski.kamil.torexitaddresses.parser.ExitNode;

import java.util.Collection;

@RestController
@RequestMapping("/{ip}")
@AllArgsConstructor
public class IpAddressController {

    private final ExitAddressesService exitAddressesService;

    @GetMapping
    public Collection<ExitNode> findIpAddress(@PathVariable("ip") IpAddress ip) {
        return exitAddressesService.findByIp(ip.getIp());
    }

    @ExceptionHandler(ExitAddressNotFoundException.class)
    public ResponseEntity<?> addressNotFound() {
        return ResponseEntity.notFound().build();
    }

}

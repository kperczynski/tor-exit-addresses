package perczynski.kamil.torexitaddresses;

import perczynski.kamil.torexitaddresses.parser.ExitNode;

import java.util.List;
import java.util.Set;

public interface ExitAddressesRepository {

    List<ExitNode> findByIp(String ip);

    void save(Set<ExitNode> exitNodes);

    int count();
}

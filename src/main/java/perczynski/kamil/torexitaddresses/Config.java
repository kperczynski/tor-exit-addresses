package perczynski.kamil.torexitaddresses;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;
import perczynski.kamil.torexitaddresses.fetching.ExitAddresses;

@Configuration
public class Config {

    @Bean
    @ExitAddresses
    public RestTemplate exitAddressesRestTemplate(RestTemplateBuilder builder) {
        return builder.setConnectTimeout(300)
                .setReadTimeout(300)
                .build();
    }

    @Configuration
    @EnableScheduling
    @Profile("!test")
    public static class SchedulerConfig {
    }

}

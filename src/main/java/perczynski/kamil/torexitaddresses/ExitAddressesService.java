package perczynski.kamil.torexitaddresses;

import org.springframework.stereotype.Service;
import perczynski.kamil.torexitaddresses.parser.ExitNode;

import java.util.Collection;
import java.util.List;

@Service
public class ExitAddressesService {

    private final ExitAddressesRepository exitAddressesRepository;

    public ExitAddressesService(ExitAddressesRepository exitAddressesRepository) {
        this.exitAddressesRepository = exitAddressesRepository;
    }

    public Collection<ExitNode> findByIp(String ip) {
        List<ExitNode> nodes = exitAddressesRepository.findByIp(ip);
        if (nodes.isEmpty())
            throw new ExitAddressNotFoundException(ip);
        return nodes;
    }
}

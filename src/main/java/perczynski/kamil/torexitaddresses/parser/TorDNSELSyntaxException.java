package perczynski.kamil.torexitaddresses.parser;

public class TorDNSELSyntaxException extends RuntimeException {

    public TorDNSELSyntaxException(String message) {
        super(message);
    }

    public TorDNSELSyntaxException(String message, Throwable cause) {
        super(message, cause);
    }

    public TorDNSELSyntaxException(Throwable cause) {
        super(cause);
    }
}

package perczynski.kamil.torexitaddresses.parser;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeParseException;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

/**
 * Parser of TOR exit nodes document compliant with TorDNSEL Exit List Format.
 * https://www.torproject.org/tordnsel/exitlist-spec.txt
 * <p>
 * Following class assumes that "ExitAddress" lines contain only IPv4 addresses.
 */
public class ExitNodesParser {

    private static final String EXIT_NODE = "ExitNode";
    private static final String PUBLISHED = "Published";
    private static final String LAST_STATUS = "LastStatus";
    private static final String EXIT_ADDRESS = "ExitAddress";
    private static final Pattern NODE_ID_PATTERN = Pattern.compile("[0-9A-F]+");
    private static final Pattern IPV4_PATTERN = Pattern.compile("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}");

    private final StringTokenizer tokenizer;

    public ExitNodesParser(String text) {
        this.tokenizer = new StringTokenizer(text);
    }

    public Set<ExitNode> parse() {
        try {
            expectNextToken(EXIT_NODE);
            Set<ExitNode> nodes = new HashSet<>();
            while (tokenizer.hasMoreTokens()) {
                ExitNode node = parseExitNode();
                nodes.add(node);
            }
            return nodes;
        } catch (NoSuchElementException e) {
            throw new TorDNSELSyntaxException("Unexpected end of the document.", e);
        } catch (DateTimeParseException e) {
            throw new TorDNSELSyntaxException("Cannot parse date or time [" + e.getParsedString() + "]. " +
                    "Dates must be YYYY-MM-DD formatted - accordingly time must be HH:mm:ss formatted", e);
        }
    }

    private ExitNode parseExitNode() {
        String nodeId = parseNodeId();

        expectNextToken(PUBLISHED);
        Instant published = parseTimestamp();

        expectNextToken(LAST_STATUS);
        Instant lastStatus = parseTimestamp();

        Set<ExitAddress> addresses = parseExitAddresses();
        return new ExitNode(nodeId, published, lastStatus, addresses);
    }

    private Set<ExitAddress> parseExitAddresses() {
        expectNextToken(EXIT_ADDRESS);
        Set<ExitAddress> addresses = new HashSet<>();
        do {
            ExitAddress exitAddress = parseExitAddress();
            if (!addresses.add(exitAddress)) {
                throw new TorDNSELSyntaxException("Duplicate exit address " + exitAddress.getIp());
            }
            // we reached to the end of document
            if (!tokenizer.hasMoreTokens()) {
                break;
            }
            // we expect next exit node or another exit address
            String nextToken = tokenizer.nextToken();
            if (nextToken.equals(EXIT_NODE)) {
                break;
            }
            if (!nextToken.equals(EXIT_ADDRESS)) {
                throw new TorDNSELSyntaxException("Syntax error. Expected [" + EXIT_ADDRESS + "] but was [" + nextToken + ']');
            }
        }
        while (tokenizer.hasMoreTokens());
        return addresses;
    }

    private String parseNodeId() {
        String nodeId = tokenizer.nextToken();
        if (!NODE_ID_PATTERN.matcher(nodeId).matches()) {
            throw new TorDNSELSyntaxException("Illegal characters in node id [" + nodeId + "]. Node id should contain only numbers and uppercase HEX characters.");
        }
        return nodeId;
    }

    private ExitAddress parseExitAddress() {
        String ip = tokenizer.nextToken();
        if (!IPV4_PATTERN.matcher(ip).matches()) {
            throw new TorDNSELSyntaxException("Invalid IP address: " + ip);
        }
        Instant time = parseTimestamp();
        return new ExitAddress(ip, time);
    }

    private void expectNextToken(String expected) {
        String token = tokenizer.nextToken();
        if (!token.equals(expected))
            throw new TorDNSELSyntaxException("Syntax error. Expected [" + expected + "] but was [" + token + ']');
    }

    private Instant parseTimestamp() {
        LocalDate date = LocalDate.parse(tokenizer.nextToken());
        LocalTime time = LocalTime.parse(tokenizer.nextToken());
        return date.atTime(time).toInstant(ZoneOffset.UTC);
    }
}
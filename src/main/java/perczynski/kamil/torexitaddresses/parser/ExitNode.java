package perczynski.kamil.torexitaddresses.parser;

import lombok.Value;

import java.time.Instant;
import java.util.Set;

@Value
public class ExitNode {

    private final String nodeId;
    private final Instant published;
    private final Instant lastStatus;
    private final Set<ExitAddress> addresses;

}

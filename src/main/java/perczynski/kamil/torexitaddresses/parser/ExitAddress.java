package perczynski.kamil.torexitaddresses.parser;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.Instant;
import java.util.Objects;

@Getter
@AllArgsConstructor
public class ExitAddress {

    private final String ip;
    private final Instant testTime;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExitAddress)) return false;
        ExitAddress that = (ExitAddress) o;
        return Objects.equals(getIp(), that.getIp());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIp());
    }
}

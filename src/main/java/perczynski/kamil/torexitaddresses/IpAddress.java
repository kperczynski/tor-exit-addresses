package perczynski.kamil.torexitaddresses;

import lombok.Getter;

import java.util.Arrays;

@Getter
public class IpAddress {

    private final String ip;

    public static IpAddress of(String ip) {
        String[] parts = ip.split("\\.");
        if (parts.length != 4) {
            throw new IllegalArgumentException("errors.ip-address-invalid");
        }
        if (Arrays.stream(parts)
                .mapToInt(Integer::parseInt)
                .anyMatch(part -> part < 0 || part > 255)) {
            throw new IllegalArgumentException("errors.ip-address-invalid");
        }
        return new IpAddress(ip);
    }

    private IpAddress(String ip) {
        this.ip = ip;
    }
}
